#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main() {
	char str[1000];
	printf("Enter a string: \n");
	fgets(str, 1000, stdin);

	char word[1000];
	printf("Enter a word to search: \n");
	fgets(word, 1000, stdin);

	int len = strlen(str) - 1;
	int wordLen = strlen(word) - 1;

	printf("\n");
	for (int i = 0; i < len; ++i) {
		int index = 0;
		if ((str[i - 1] == ' ' && str[i] != ' ' && i <= len - wordLen) || i == 0) {
			for (int j = 0; j < wordLen; ++j) {
				if (str[i + j] == word[j])
					index++;
				else
					break;

				if (j == wordLen - 1)
					if (i + j + 1 != len && str[i + j + 1] != ' ')
						index--;
			}
		}
		if (index == wordLen) {
			printf("The word is in string.\n");
			return 0;
		}
	}

	printf("The word is not in string.\n");
}