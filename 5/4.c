#include <stdio.h>

int main() {
	int size;

	printf("Enter size of array: \n");
	scanf("%d", &size);

	int arr[size];

	for (int i = 0; i < size; ++i) {
		printf("element %d = ", i + 1);
		scanf("%d", &arr[i]);
	}


	int newArr[size - 1];

	int deleteElementPosition = 0;
	printf("Delete element position: \n");
	scanf("%d", &deleteElementPosition);

	for (int i = 0; i < size - 1; ++i) {
		if (i >= deleteElementPosition - 1) {
			newArr[i] = arr[i + 1];
		} else {
			newArr[i] = arr[i];
		}
	}

	printf("\n");
	printf("New: \n");

	for (int i = 0; i < size - 1; ++i) {
		printf("%d ", newArr[i]);
	}
	printf("\n");
}
