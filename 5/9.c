#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main() {
	char str[1000];

	printf("Enter a string: \n");
	fgets(str, 1000, stdin);

	int len = strlen(str) - 1;
	int wordsCount = 0;

	for (int i = 0; i < len; ++i) {
		if ((str[i - 1] == ' ' && str[i] != ' ') || i == 0)
			str[i] = toupper(str[i]);
	}

	printf("\n");
	printf("%s\n", str);
}