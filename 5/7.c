#include <stdio.h>
#include <string.h>

int main() {
	char str[1000];

	printf("Enter a string: \n");
	fgets(str, 1000, stdin);

	int len = strlen(str) - 1;
	int wordsCount = 0;

	for (int i = 0; i < len - 1; ++i) {
		if (str[i] == ' ' && str[i + 1] != ' ')
			wordsCount++;
	}

	printf("\n");
	printf("Words count: %d\n", wordsCount + 1);
}