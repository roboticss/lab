#include <stdio.h>

int inArray(int, int[], int);

int main() {
	int size;

	printf("Enter size of array: \n");
	scanf("%d", &size);

	int arr[size];
	int countOfUnique = 0;
	int unique[size];

	for (int i = 0; i < size; ++i) {
		printf("element %d = ", i + 1);
		scanf("%d", &arr[i]);
		if (!inArray(arr[i], arr, i)) {
			unique[countOfUnique] = arr[i];
			countOfUnique++;
		}
	}


	printf("\n");
	printf("Result: \n");

	for (int i = 0; i < countOfUnique; ++i) {
		int count = 0;
		for (int j = 0; j < size; ++j) {
			if (unique[i] == arr[j])
				count++;
		}

		printf("%d   -> %d times\n", unique[i], count);
	}
	printf("\n");
}


int inArray(int val, int *arr, int size){
	for (int i = 0; i < size; i++)
		if (arr[i] == val)
			return 1;
	return 0;
}