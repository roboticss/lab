#include <stdio.h>

int main() {
	int size;

	printf("Enter size of array: \n");
	scanf("%d", &size);

	int arr[size];

	for (int i = 0; i < size; ++i) {
		printf("element %d = ", i + 1);
		scanf("%d", &arr[i]);
	}

	int countModTwo = 0;

	for (int i = 0; i < size; ++i) {
		if (arr[i] % 2 == 0)
			countModTwo++;
	}

	int arrModTwo[countModTwo];
	int arrNotModTwo[size - countModTwo];

	int cmtNext = 0;
	int cnmtNext = 0;

	for (int i = 0; i < size; ++i) {
		if (arr[i] % 2 == 0) {
			arrModTwo[cmtNext] = arr[i];
			cmtNext++;
		} else {
			arrNotModTwo[cnmtNext] = arr[i];
			cnmtNext++;
		}
	}

	printf("\n\n");
	printf("Result: \n\n");

	if (countModTwo > 0)
		printf("arr % 2 == 0 : \n");
	for (int i = 0; i < countModTwo; ++i) {
		printf("%d ", arrModTwo[i]);
	}
	printf("\n");

	if (countModTwo < size)
		printf("arr % 2 != 0 : \n");
	for (int i = 0; i < size - countModTwo; ++i) {
		printf("%d ", arrNotModTwo[i]);
	}
	printf("\n");
}
