#include <stdio.h>
#include <math.h>

int main() {
	int number = 0, c = 2;


	printf("number = ");
	scanf("%d", &number);

	while (c <= sqrt(number)) {
		if (number % c == 0) {
			printf("The number isn't prime\n");
			return 0;
		}
		c++;
	}
	
	if (number < 2)
		printf("The number isn't prime\n");
	else
		printf("The number is prime\n");
	return 0;
}
