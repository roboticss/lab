#include <stdio.h>

int main() {
	int number = 0;

	int count = 0,
		min = 1,
		max = 0,
		sum = 0;
		

	printf("Type a number: ");
	scanf("%d", &number);

	count++;
	sum += number;
	min = number;
	max = number;

	do {
		printf("Type a next number: ");
		scanf("%d", &number);

		if (number <= 0)
			break;

		count++;
		sum += number;
		if (min > number)
			min = number;
		if (max < number)
			max = number;

	} while (number > 0);


	printf("\n");
	printf("Result: \n");

	printf("  count: %d\n", count);
	printf("  min: %d\n", min);
	printf("  max: %d\n", max);
	printf("  avg: %f\n", (float)sum / count);
}
