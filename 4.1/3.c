#include <stdio.h>

#define MAX 10

int main() {
	int n, a, bi;

	n = 0;

	while (n <= 10) {
		printf(" %d  ", n);
		++n;
	}

	printf("\n-------------------------------------------\n");
	n = 0;


	for (int i = 0; i < 10; ++i) {
		a = 0;
		bi = 1;
		printf("%2d", n);

		while (a <= n) {
			if (n == 0 || a == 0) {
				printf("%4d", bi);
			} else {
				bi = bi * (n - a + 1) / a;
				printf("%4d", bi);
			}

			a += 1;
		}

		printf("\n");
		n += 1;

		if (n > MAX)
			break;
	}

	printf("-------------------------------------------\n");

}
