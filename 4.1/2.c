#include <stdio.h>

int main() {
	int n, i = 0;
	double x, y = 1;

	printf("x = ");
	scanf("%lf", &x);

	printf("n = ");
	scanf("%d", &n);

	do {
		y *= x;
		i++;
	} while (i < n);
	
	printf("\n");
	printf("y = %lf\n", y);
}
