#include <stdio.h>
#include <math.h>

int main() {
	double a, b, c;

	printf("A: ");
	scanf("%lf", &a);

	printf("B: ");
	scanf("%lf", &b);

	printf("C: ");
	scanf("%lf", &c);

	double x1 = (-b + sqrt(pow(b, 2) - 4 * a * c)) / (2 * a);
	double x2 = (-b - sqrt(pow(b, 2) - 4 * a * c)) / (2 * a);

	printf("x1 = %lf\nx2 = %lf\n", x1, x2);

	return 0;
}
