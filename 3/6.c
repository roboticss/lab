#include <stdio.h>
#include <math.h>

int main() {
	unsigned int a, b;

	printf("A: ");
	scanf("%d", &a);

	printf("B: ");
	scanf("%d", &b);

	if (
		(a % b == 0) ||
		(b % a == 0)
	) {
		printf("Числа кратні.\n");
	}

	return 0;
}
