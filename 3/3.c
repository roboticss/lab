#include <stdio.h>
#include <math.h>

int main() {
	int a;

	printf("A: ");
	scanf("%d", &a);

	if (a < 0 || a > 80) {
		printf("! a > 80 or a < 0 !\n");
	} else {
		int range = (a / 10 + 2) * 10;
		printf("range: [%d, %d]\n", a, range);
	}

	return 0;
}
