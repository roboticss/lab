#include <stdio.h>

int main() {
	int l, w;
	int P, S;
	
	l = 5;
	w = 7;
	
	P = 2 * (l + w);
	S = l * w;
	
	printf("P: %d sm\nS: %d sm^2\n", P, S);
}
