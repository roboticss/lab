#include <stdio.h>

int main (int argc, char *argv[]) {
	char *lines[] = {
		"  #######",
		" ##    ##",
		"##",
		"#",
		"#",
		"#",
		"##",
		" ##    ##",
		"  #######"
	};
	
	for (int i = 0; i < 9; i++) {
		printf("%s\n", lines[i]);
	}
																		
	return 0;
}
