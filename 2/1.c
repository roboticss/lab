#include <stdio.h>

int main()
{
	int a, b, sum, dob;

	printf("A: ");
	scanf("%d", &a);

	printf("B: ");
	scanf("%d", &b);

	sum = a + b;
	dob = a * b;

	printf("----\n");
	printf("sum = %d\ndob = %d\n", sum, dob);
}

