#include <stdio.h>

int main() {

	int upperZeroSum = 0;
	int underZeroMultipl = 1;

	for (int i = 0; i < 5; i++) {
		int number = 0;
		printf("type %d number: ", i + 1);
		scanf("%d", &number);

		if (number > 0)
			upperZeroSum += number;
		else if (number < 0)
			underZeroMultipl *= number;
	}

	printf("\n");
	printf("Sum > 0 : %d\n", upperZeroSum);
	printf("Multipl < 0 : %d\n", underZeroMultipy);
}
