#include <stdio.h>
#include <math.h>

int main() {

	int start, end;
	int sum = 0;

	printf("Start: ");
	scanf("%d", &start);

	printf("End: ");
	scanf("%d", &end);

	printf("\nResult: ");
	for (int i = start; i <= end; ++i) {
		if (i % 7 == 2 || i % 7 == 3) {
			printf("%d ", i);
			sum += i;
		}
	}

	printf("\nSum: %d\n", sum);
}
