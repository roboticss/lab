#include <stdio.h>

int main() {

	int underZero = 0;
	int upperZero = 0;

	for (int i = 0; i < 5; i++) {
		int number = 0;
		printf("type %d number: ", i + 1);
		scanf("%d", &number);

		if (number > 0)
			upperZero++;
		else if (number < 0)
			underZero++;
	}

	printf("\n");
	printf("> 0 : %d\n", upperZero);
	printf("< 0 : %d\n", underZero);
}
