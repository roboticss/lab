#include <stdio.h>
#include <math.h>

int main() {

	int max = 0;
	int position = 0;

	printf("Type a 5 numbers: \n");
	scanf("%d", &max);
	
	for (int i = 0; i < 4; i++) {
		int tmp = 0;
		scanf("%d", &tmp);

		if (tmp > max) {
			max = tmp;
			position = i + 1;
		}
	}

	printf("\n");
	printf("Max: %d\n", max);
	printf("Position: %d\n", position);
}
