#include <stdio.h>
#include <math.h>

int main() {
	int number = 0;

	printf("Number: ");
	scanf("%d", &number);

	printf("\n");

	for (int i = 2; i <= (number % 2 == 0 ? number : number - 1); i += 2) {
		int calc = pow(i, 2);
		printf("%d^2 = %d\n", i, calc);
	}
}
