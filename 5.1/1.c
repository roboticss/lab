#include <stdio.h>

int main() {
	int row;
	int col;


	printf("Rows: ");
	scanf("%d", &row);

	printf("Cols: ");
	scanf("%d", &col);

	int first[row][col];
	int second[row][col];

	// scan first matrix
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("element of first matrix [%d][%d] = ", i, j);
			scanf("%d", &first[i][j]);
		}
	}

	// scan second matrix
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("element of second matrix [%d][%d] = ", i, j);
			scanf("%d", &second[i][j]);
		}
	}

	printf("First matrix: \n");
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("%d\t", first[i][j]);
		}
		printf("\n");
	}

	printf("Second matrix: \n");
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("%d\t", second[i][j]);
		}
		printf("\n");
	}

	int summed[row][col];

	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			summed[i][j] = first[i][j] + second[i][j];
		}
	}

	printf("\n");
	printf("Sum: \n");
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("%d\t", summed[i][j]);
		}
		printf("\n");
	}
}
