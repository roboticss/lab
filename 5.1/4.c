#include <stdio.h>

int main() {
	int row;
	int col;

	printf("Rows: ");
	scanf("%d", &row);

	printf("Cols: ");
	scanf("%d", &col);

	int matrix[row][col];

	// scan first matrix
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("element of matrix [%d][%d] = ", i, j);
			scanf("%d", &matrix[i][j]);
		}
	}

	printf("Matrix: \n");
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("%d\t", matrix[i][j]);
		}
		printf("\n");
	}

	int tMatrix[col][row];

	for (int i = 0; i < col; ++i) {
		for (int j = 0; j < row; ++j) {
			tMatrix[i][j] = matrix[j][i];
		}
	}

	printf("\n");
	printf("TMatrix: \n");
	for (int i = 0; i < col; ++i) {
		for (int j = 0; j < row; ++j) {
			printf("%d\t", tMatrix[i][j]);
		}
		printf("\n");
	}

}
