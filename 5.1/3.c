#include <stdio.h>

int main() {
	int row;
	int col;

	printf("Rows: ");
	scanf("%d", &row);

	printf("Cols: ");
	scanf("%d", &col);

	int matrix[row][col];

	// scan first matrix
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("element of matrix [%d][%d] = ", i, j);
			scanf("%d", &matrix[i][j]);
		}
	}

	printf("Matrix: \n");
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("%d\t", matrix[i][j]);
		}
		printf("\n");
	}

	int sumOfMainDiagonal = 0;
	int sumOfOppositeDiagonal = 0;

	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			if (i == j) sumOfMainDiagonal += matrix[i][j];
			if (row == j + i + 1) sumOfOppositeDiagonal += matrix[i][j];
		}
	}

	printf("\n");
	printf("Sum of main: %d\n", sumOfMainDiagonal);
	printf("Sum of opposite: %d\n", sumOfOppositeDiagonal);

}
